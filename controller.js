'use strict';

var response = require('./rest');
var connection = require('./koneksi');

exports.index = function (req, res) {
    response.ok("Aplikasi berjalan", res);
}

//menampilkan semua data mahasiswa
exports.tampilsemuamahasiswa = function (req, res) {
    connection.query('SELECT * FROM mahasiswa',
        function (error, rows, fileds) {

            if (error) {
                console.log(error);
            } else {
                response.ok(rows, res);
            }

        });
}

exports.tampilberdasarkanid = function (req, res) {
    let id = req.params.id;
    connection.query('SELECT * FROM mahasiswa WHERE id_mahasiswa = ?', [id],
        function (error, rows, fiels) {

            if (error) {
                console.log(error);
            } else {
                response.ok(rows, res);
            }

        });
}

//Menambahkan data mahasiswa
exports.tambahMahasiswa = function (req, res) {
    var nim = req.body.nim;
    var nama = req.body.nama;
    var jurusan = req.body.jurusan;

    connection.query('INSERT INTO mahasiswa (nim, nama, jurusan) VALUES(?,?,?)',
        [nim, nama, jurusan],
        function (error, rows, fiels) {

            if (error) {
                console.log(error);
            } else {
                response.ok('Berhasil menambahkan data', res);
            }

        });
};

exports.mengubahMahasiswa = function (req, res) {
    var id = req.body.id_mahasiswa;
    var nim = req.body.nim;
    var nama = req.body.nama;
    var jurusan = req.body.jurusan;

    connection.query('UPDATE mahasiswa SET nim=?, nama=?, jurusan=? WHERE id_mahasiswa = ?',
        [nim, nama, jurusan, id],
        function (error, rows, fiels) {

            if (error) {
                console.log(error);
            } else {
                response.ok('Berhasil mengubah data', res);
            }

        }
    )
}

exports.hapusMahasiswa = function (req, res) {
    var id = req.body.id_mahasiswa;
    connection.query('DELETE FROM mahasiswa WHERE id_mahasiswa = ?', [id],
        function (error, rows, fiels) {

            if (error) {
                console.log(error);
            } else {
                response.ok('Berhasil menghapus data', res);
            }

        });
}


